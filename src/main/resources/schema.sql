DROP TABLE IF EXISTS borrowing_registry;
DROP TABLE IF EXISTS books_categories;
DROP TABLE IF EXISTS books_authors;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS users;

create table users
(
    id         bigint auto_increment not null,
    card_number varchar(255) null,
    name       varchar(255) not null,
    surname    varchar(255) not null,
    email      varchar(255) not null,
    age        int not null,
    book_counter int not null  default 0,
    password   varchar(255) null,
    PRIMARY KEY (id)
);
create table books
(
    id            bigint auto_increment,
    title         varchar(255) not null,
    is_available  BOOLEAN DEFAULT true,
    borrowing_date date null,
    current_borrower_id bigint null,

    PRIMARY KEY (id)
);

create table categories
(
    id            bigint auto_increment,
    type         varchar(255) not null,
    PRIMARY KEY (id)
);

create table borrowing_registry
(
    id            bigint auto_increment,
    borrowing_date date null,
    due_date       date null,
    users_id       bigint not null,
    book_id       bigint not null,
    PRIMARY KEY (id)
);

create table books_categories
(
    book_id    bigint not null,
    category_id bigint not null
);

create table authors
(
    id      bigint auto_increment,
    name    varchar(255) not null,
    surname varchar(255) not null,
    PRIMARY KEY (id)
);

create table books_authors
(
    book_id   bigint not null,
    author_id bigint not null
);

alter table books
    add constraint FK_Book_users foreign key (current_borrower_id) references users(id) on delete no action;

alter table borrowing_registry
    add constraint FK_users_Rental foreign key (users_id) references users (id) on delete no action;

alter table borrowing_registry
    add constraint FK_Book_Rental foreign key (book_id) references books  (id) on delete no action;

alter table books_authors
    add constraint FK_Authors_Books foreign key (author_id) references authors (id)  on delete no action;

alter table books_authors
    add constraint FK_Book_Authors foreign key (book_id) references books (id) on delete no action;

alter table books_categories
    add constraint FK_Books_Categories foreign key (book_id) references books (id) on delete no action;

alter table books_categories
    add constraint FK_Categories_Books foreign key (category_id) references categories (id) on delete no action;