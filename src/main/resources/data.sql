INSERT INTO users(id, card_number, name, surname, email, age, book_counter, password)
VALUES (1, 1039868190, 'Mateusz', 'Potempa', 'empsi15@gmail.com', 18,0, 'Mateusz');

INSERT INTO users(id, card_number, name, surname, email, age,book_counter, password)
VALUES (2, 1117311650, 'Kamil', 'Murawski', 'Kamil20@gmail.com', 12,2, 'Kamil');

INSERT INTO users(id, card_number, name, surname, email, age,book_counter, password)
VALUES (3, 1168582595, 'Mariusz', 'Marski', 'Marski30@gmail.com', 30,1, 'Mariusz');

INSERT INTO users(id, card_number, name, surname, email, age,book_counter, password)
VALUES (4, 1231320107, 'Wojciech', 'Wojtaz', 'Wojtaz56@gmail.com', 56,0, 'Wojciech');

INSERT INTO users(id, card_number, name, surname, email, age,book_counter, password)
VALUES (5, 1231320108, 'Kamil', 'Kozel', 'Kamil21@gmail.com', 25,0, 'Kamil');

INSERT INTO users(id, card_number, name, surname, email, age,book_counter, password)
VALUES (6, 1231320109, 'Jaroslaw', 'Jak', 'Jaroslaw54@gmail.com', 10, 1,'Jaroslaw');

INSERT INTO users(id, card_number, name, surname, email, age,book_counter, password)
VALUES (7, 1231320102, 'Mariusz', 'Kaz', 'Mariusz12@gmail.com', 40,1, 'Mariusz');

INSERT INTO users(id, card_number, name, surname, email, age,book_counter, password)
VALUES (8, 1231320101, 'Aleksandra', 'Aleks', 'Aleksandra101@gmail.com',19, 1, 'Aleksandra');


INSERT INTO books(id, title, is_available,borrowing_date,current_borrower_id)
VALUES (1, 'Win', true,null,null);

INSERT INTO books(id, title, is_available,borrowing_date,current_borrower_id)
VALUES (2, 'Home', false,'2021-07-16',2);

INSERT INTO books(id, title, is_available,borrowing_date,current_borrower_id)
VALUES (3, 'IT', false,'2021-07-20',2);

INSERT INTO books(id, title, is_available,borrowing_date,current_borrower_id)
VALUES (4, 'The Man in Brown Suit', true,null,null);

INSERT INTO books(id, title, is_available,borrowing_date,current_borrower_id)
VALUES (5, 'The Big Four', false,'2021-07-20',8);

INSERT INTO books(id, title, is_available,borrowing_date,current_borrower_id)
VALUES (6, 'Carrie', false,'2021-06-01',7);

INSERT INTO books(id, title, is_available,borrowing_date,current_borrower_id)
VALUES (7, 'The Lincoln Myth', false,null,null);

INSERT INTO books(id, title, is_available,borrowing_date,current_borrower_id)
VALUES (8, 'Jefferson Key', false,null,null);

INSERT INTO books(id, title, is_available,borrowing_date,current_borrower_id)
VALUES (9, 'Origin', false,'2021-07-20',3);

INSERT INTO books(id, title, is_available,borrowing_date,current_borrower_id)
VALUES (10, 'The Da Vinci Code', false,'2021-07-20',6);


INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (1,  '2021-01-01', '2021-02-01',1, 5);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (2,  '2021-01-01', '2021-02-01',1, 1);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (3, '2021-02-01', '2021-03-01',2, 2);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (4,'2021-02-01', '2021-03-01',10, 6);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (5,  '2021-03-01', '2021-04-01',2, 4);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (6,  '2021-03-01', '2021-04-01',1, 4);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (7, '2021-04-01', '2021-05-01',2, 4);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (8,  '2021-06-02', '2021-07-01',3, 2);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (9,'2021-06-01', '2021-07-01',6, 5);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (10,  '2021-07-10', '2021-08-10',5, 8);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (11,'2021-07-16', '2021-08-16',2, 2);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (12, '2021-07-20', '2021-08-20',3, 2);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (13,'2021-07-20', '2021-08-20',9, 3);

INSERT INTO borrowing_registry(id, borrowing_date, due_date,book_id, users_id)
VALUES (14, '2021-07-20', '2021-08-20',10, 6);



INSERT INTO authors(id, name, surname)
VALUES (1, 'Harlan', 'Coben');

INSERT INTO authors(id, name, surname)
VALUES (2, 'Stephen', 'King');

INSERT INTO authors(id, name, surname)
VALUES (3, 'Agatha', 'Christie');

INSERT INTO authors(id, name, surname)
VALUES (4, 'Steve', 'Berry');

INSERT INTO authors(id, name, surname)
VALUES (5, 'Dan', 'Brown');


INSERT INTO  categories(id,type)
values (1,'FICTION');

INSERT INTO  categories(id,type)
values (2,'HISTORICAL_FICTION');

INSERT INTO  categories(id,type)
values (3,'HISTORICAL_NON_FICTION');

INSERT INTO  categories(id,type)
values (4,'BIOGRAPHY');

INSERT INTO  categories(id,type)
values (5,'DRAMA');

INSERT INTO  categories(id,type)
values (6,'LOVE_STORIES');

INSERT INTO  categories(id,type)
values (7,'JUVENILE_LITERATURE');

INSERT INTO  categories(id,type)
values (8,'KIDS_LITERATURE');

INSERT INTO  categories(id,type)
values (9,'GRAPHIC_NOVELS');

INSERT INTO  categories(id,type)
values (10,'ELECTRONIC');



INSERT INTO books_categories(book_id,category_id)
VALUES (1, 5);

INSERT INTO books_categories(book_id,category_id)
VALUES (1, 10);

INSERT INTO books_categories(book_id,category_id)
VALUES (2, 6);

INSERT INTO books_categories(book_id,category_id)
VALUES (2, 4);

INSERT INTO books_categories(book_id,category_id)
VALUES (3, 3);

INSERT INTO books_categories(book_id,category_id)
VALUES (3, 7);

INSERT INTO books_categories(book_id,category_id)
VALUES (4, 8);

INSERT INTO books_categories(book_id,category_id)
VALUES (4, 2);

INSERT INTO books_categories(book_id,category_id)
VALUES (5, 1);

INSERT INTO books_categories(book_id,category_id)
VALUES (5, 2);

INSERT INTO books_categories(book_id,category_id)
VALUES (5, 3);

INSERT INTO books_categories(book_id,category_id)
VALUES (6, 4);

INSERT INTO books_categories(book_id,category_id)
VALUES (6, 5);

INSERT INTO books_categories(book_id,category_id)
VALUES (7, 6);

INSERT INTO books_categories(book_id,category_id)
VALUES (7, 6);

INSERT INTO books_categories(book_id,category_id)
VALUES (8, 8);

INSERT INTO books_categories(book_id,category_id)
VALUES (8, 9);

INSERT INTO books_categories(book_id,category_id)
VALUES (9, 10);

INSERT INTO books_categories(book_id,category_id)
VALUES (9, 1);

INSERT INTO books_categories(book_id,category_id)
VALUES (10, 2);

INSERT INTO books_categories(book_id,category_id)
VALUES (10, 3);


INSERT INTO books_authors(book_id, author_id)
VALUES (1, 1);

INSERT INTO books_authors(book_id, author_id)
VALUES (2, 1);

INSERT INTO books_authors(book_id, author_id)
VALUES (3, 2);

INSERT INTO books_authors(book_id, author_id)
VALUES (4, 2);

INSERT INTO books_authors(book_id, author_id)
VALUES (5, 3);

INSERT INTO books_authors(book_id, author_id)
VALUES (6, 3);

INSERT INTO books_authors(book_id, author_id)
VALUES (7, 4);

INSERT INTO books_authors(book_id, author_id)
VALUES (8, 4);

INSERT INTO books_authors(book_id, author_id)
VALUES (9, 5);

INSERT INTO books_authors(book_id, author_id)
VALUES (10, 5);