package com.library.libraryrentalservice.service;

import com.library.libraryrentalservice.exception.ErrorCode;
import com.library.libraryrentalservice.exception.LibraryBorrowingException;
import com.library.libraryrentalservice.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CategoryService {
    
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Set<String> getAllBookCategories(){

        Set<String>foundBookCategories=categoryRepository.findAllCategories();

        if(foundBookCategories.isEmpty()){
            throw new LibraryBorrowingException(ErrorCode.NO_CATEGORY_IN_TABLE);
        }

        return foundBookCategories;
    }
}

