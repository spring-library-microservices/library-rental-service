package com.library.libraryrentalservice.service;

import com.library.libraryrentalservice.dto.UserResponse;
import com.library.libraryrentalservice.exception.ErrorCode;
import com.library.libraryrentalservice.exception.LibraryBorrowingException;
import com.library.libraryrentalservice.mapper.UserMapper;
import com.library.libraryrentalservice.model.User;
import com.library.libraryrentalservice.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public Set<UserResponse> getUsersWithNoBorrowingRecord() {

        Set<User> foundUsers = userRepository.usersWithNoBorrowingRecord();

        if (foundUsers.isEmpty()) {
            throw new LibraryBorrowingException(ErrorCode.NO_LAZY_USER);
        }
        return foundUsers
                .stream()
                .map(userMapper::toDto)
                .collect(toSet());
    }
}
