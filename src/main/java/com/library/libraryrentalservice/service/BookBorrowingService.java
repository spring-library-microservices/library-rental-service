package com.library.libraryrentalservice.service;

import com.library.libraryrentalservice.exception.ErrorCode;
import com.library.libraryrentalservice.exception.LibraryBorrowingException;
import com.library.libraryrentalservice.model.Book;
import com.library.libraryrentalservice.model.User;
import com.library.libraryrentalservice.repository.BookBorrowingRegistryRepository;
import com.library.libraryrentalservice.repository.BookRepository;
import com.library.libraryrentalservice.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Objects;

@Service
public class BookBorrowingService {

    private final static long DUE_DATE = 30;
    private final static int UPPER_LIMIT = 7;
    private final static int MINIMAL_LIMIT = 0;

    private final BookRepository bookRepository;
    private final BookBorrowingRegistryRepository bookBorrowingRegistryRepository;
    private final UserRepository userRepository;


    public BookBorrowingService(BookRepository bookRepository, BookBorrowingRegistryRepository bookBorrowingRegistryRepository, UserRepository userRepository) {
        this.bookRepository = bookRepository;
        this.bookBorrowingRegistryRepository = bookBorrowingRegistryRepository;
        this.userRepository = userRepository;
    }

    public void borrowBook(long bookId, long userId) {

        Book foundBook = bookRepository.findBookById(bookId);

        if (Objects.isNull(foundBook)) {
            throw new LibraryBorrowingException(ErrorCode.BOOK_NOT_FOUND);
        }

        if (!foundBook.isAvailable()) {
            throw new LibraryBorrowingException(ErrorCode.BOOK_UNAVAILABLE);
        }

        User foundUser = userRepository.findUserById(userId);

        if (Objects.isNull(foundUser)) {
            throw new LibraryBorrowingException(ErrorCode.USER_NOT_FOUND);
        }

        bookRepository.changeAvailabilityAndCurrentBorrower(false, foundBook, foundUser, LocalDate.now());

        int userBookCount = foundUser.getBookCounter();

        if (userBookCount >= MINIMAL_LIMIT && userBookCount < UPPER_LIMIT) {
            userBookCount++;
        } else {
            throw new LibraryBorrowingException(ErrorCode.BOOK_BORROWING_LIMIT);
        }
        userRepository.updateUserBookCounter(foundUser, userBookCount);

        bookBorrowingRegistryRepository
                .saveBorrowingRecord(LocalDate.now(), LocalDate.now().plusDays(DUE_DATE), foundBook.getId(), foundUser.getId());
    }

    public void returnBook(long bookId, long userId) {

        Book foundBook = bookRepository.findBookById(bookId);

        if (Objects.isNull(foundBook)) {
            throw new LibraryBorrowingException(ErrorCode.BOOK_NOT_FOUND);
        }
        if (!foundBook.isAvailable()) {

            User foundUser = userRepository.findUserById(userId);

            if (Objects.isNull(foundUser)) {
                throw new LibraryBorrowingException(ErrorCode.USER_NOT_FOUND);
            }

            if (foundBook.getCurrentBorrower().equals(foundUser)) {

                bookRepository.changeAvailabilityAndCurrentBorrower(true, foundBook, null, null);

                int userBookCount = foundUser.getBookCounter();
                userBookCount--;

                userRepository.updateUserBookCounter(foundUser, userBookCount);

            }
        }
    }
}
