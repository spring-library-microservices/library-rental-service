package com.library.libraryrentalservice.service;

import com.library.libraryrentalservice.dto.AuthorResponse;
import com.library.libraryrentalservice.exception.ErrorCode;
import com.library.libraryrentalservice.exception.LibraryBorrowingException;
import com.library.libraryrentalservice.mapper.AuthorMapper;
import com.library.libraryrentalservice.model.AgeRange;
import com.library.libraryrentalservice.model.Author;
import com.library.libraryrentalservice.repository.AuthorRepository;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class AuthorService {

    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    public AuthorService(AuthorRepository authorRepository, AuthorMapper authorMapper) {
        this.authorRepository = authorRepository;
        this.authorMapper = authorMapper;
    }

    public AuthorResponse getMostPopularAuthorForAgeGroup(AgeRange ageRange) {

        Author foundAuthor = authorRepository
                .findMostPopularAuthorForAgeGroup(ageRange.getRangeStart(), ageRange.getRangeEnd());

        if (Objects.isNull(foundAuthor)) {
            throw new LibraryBorrowingException(ErrorCode.AUTHOR_NOT_FOUND);
        }

        return authorMapper.toDto(foundAuthor);

    }
}
