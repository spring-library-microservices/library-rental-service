package com.library.libraryrentalservice.service;

import com.library.libraryrentalservice.dto.BookResponse;
import com.library.libraryrentalservice.exception.ErrorCode;
import com.library.libraryrentalservice.exception.LibraryBorrowingException;
import com.library.libraryrentalservice.mapper.BookMapper;
import com.library.libraryrentalservice.model.AgeRange;
import com.library.libraryrentalservice.model.Book;
import com.library.libraryrentalservice.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.Set;

import static java.util.stream.Collectors.toSet;


@Service
public class BookService {

    private final BookRepository bookRepository;
    private final BookMapper bookMapper;

    public BookService(BookRepository bookRepository, BookMapper bookMapper) {
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
    }


    public Set<BookResponse> getAllBooks() {

        Set<Book> foundBooks = bookRepository.findAllBooks();

        if (foundBooks.isEmpty()) {
            throw new LibraryBorrowingException(ErrorCode.BOOKS_EMPTY_TABLE);
        }

        return foundBooks
                .stream()
                .map(bookMapper::toDto)
                .collect(toSet());
    }

    public Set<BookResponse> getBooksByCategory(Set<String> bookCategories) {

        if (bookCategories.isEmpty()) {
            throw new LibraryBorrowingException(ErrorCode.CATEGORY_EMPTY_SET);
        }

        return bookRepository.findAllBooksByCategory(bookCategories)
                .stream()
                .map(bookMapper::toDto)
                .collect(toSet());
    }


    public Set<BookResponse> getBooksWithOnHandsStatus() {

        Set<Book> foundBooks = bookRepository.findAllOnHandsBooks();

        if (foundBooks.isEmpty()) {
            throw new LibraryBorrowingException(ErrorCode.NO_BOOKS_ON_HANDS);
        }

        return foundBooks
                .stream()
                .map(bookMapper::toDto)
                .collect(toSet());
    }


    public Set<BookResponse> getBooksWithOverdueStatus() {

        Set<Book> foundBooks = bookRepository.findAllOverdueBooks();

        if (foundBooks.isEmpty()) {
            throw new LibraryBorrowingException(ErrorCode.NO_BOOKS_OVERDUE);
        }
        return foundBooks
                .stream()
                .map(bookMapper::toDto)
                .collect(toSet());
    }

    public Set<BookResponse> getTop5PopularBooksInYear() {

        Set<Book> foundBooks = bookRepository.findTop5PopularBooks();

        return foundBooks
                .stream()
                .map(bookMapper::toDto)
                .collect(toSet());
    }

    public Set<BookResponse> getBooksForAgeRange(AgeRange ageRange) {

        Set<Book> foundBooks = bookRepository.findBookByAgeRange(ageRange.getRangeStart(), ageRange.getRangeEnd());

        if (foundBooks.isEmpty()) {
            throw new LibraryBorrowingException(ErrorCode.NO_BOOKS_AGE_RANGE);
        }

        return foundBooks
                .stream()
                .map(bookMapper::toDto)
                .collect(toSet());
    }


}
