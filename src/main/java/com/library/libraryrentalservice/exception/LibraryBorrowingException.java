package com.library.libraryrentalservice.exception;

public class LibraryBorrowingException extends RuntimeException{

    public LibraryBorrowingException(ErrorCode errorCode) {
        super(errorCode.message);
    }
}
