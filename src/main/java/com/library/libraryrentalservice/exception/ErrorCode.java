package com.library.libraryrentalservice.exception;

public enum ErrorCode {

    AUTHOR_NOT_FOUND("Author with given constraints do not exists"),
    BOOK_NOT_FOUND("Book with given constraints do not exists"),
    USER_NOT_FOUND("User with given id does not exists"),
    BOOK_UNAVAILABLE("Book is currently unavailable"),


    BOOK_BORROWING_LIMIT("You are allowed to have up to 7 books currently borrowed"),

    BOOKS_EMPTY_TABLE("There are no books in the database"),
    CATEGORY_EMPTY_SET("Please provide at least one category"),

    NO_CATEGORY_IN_TABLE("There are no categories in the database"),
    NO_BOOKS_ON_HANDS("All books are currently available"),
    NO_BOOKS_OVERDUE("No books are currently overdue"),
    NO_BOOKS_AGE_RANGE("There are no books for a given age range"),
    NO_LAZY_USER("There is no user with empty borrowing record");


    final String message;

    ErrorCode(String message) {
        this.message = message;
    }
}
