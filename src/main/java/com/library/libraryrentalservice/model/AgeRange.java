package com.library.libraryrentalservice.model;

public enum AgeRange {

    RANGE_BELOW_18(0, 18),
    RANGE_18_23(18, 23),
    RANGE_23_35(23, 35),
    RANGE_35_50(35, 50),
    RANGE_ABOVE_50(50, Integer.MAX_VALUE);
    
    private final int rangeStart;
    private final int rangeEnd;

    AgeRange(int rangeStart, int rangeEnd) {
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
    }

    public int getRangeStart() {
        return rangeStart;
    }

    public int getRangeEnd() {
        return rangeEnd;
    }
}
