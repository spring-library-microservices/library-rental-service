package com.library.libraryrentalservice.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;

    private boolean isAvailable;

    private LocalDate borrowingDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "books_authors",
            joinColumns = {@JoinColumn(name = "book_id")},
            inverseJoinColumns = {@JoinColumn(name = "author_id")
            }
    )
    private Set<Author> authors;

    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
    private List<BookBorrowingRegistry> bookRentalRegistry;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "books_categories",
            joinColumns = {@JoinColumn(name = "book_id")},
            inverseJoinColumns = {@JoinColumn(name = "category_id")
            }
    )
    private Set<Category> categories;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "current_borrower_id")
    @JsonIgnoreProperties("book")
    private User currentBorrower;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public LocalDate getBorrowingDate() {
        return borrowingDate;
    }

    public void setBorrowingDate(LocalDate borrowingDate) {
        this.borrowingDate = borrowingDate;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public List<BookBorrowingRegistry> getBookRentalRegistry() {
        return bookRentalRegistry;
    }

    public void setBookRentalRegistry(List<BookBorrowingRegistry> bookRentalRegistry) {
        this.bookRentalRegistry = bookRentalRegistry;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public User getCurrentBorrower() {
        return currentBorrower;
    }

    public void setCurrentBorrower(User currentBorrower) {
        this.currentBorrower = currentBorrower;
    }


}
