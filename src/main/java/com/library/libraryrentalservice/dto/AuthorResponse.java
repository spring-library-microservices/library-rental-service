package com.library.libraryrentalservice.dto;

public class AuthorResponse {

    private long id;
    private String name;
    private String surname;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    private AuthorResponse(long id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    public static class Builder {
        private long id;
        private String name;
        private String surname;

        public Builder setId(long id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public AuthorResponse build() {
            return new AuthorResponse(id, name, surname);
        }
    }
}
