package com.library.libraryrentalservice.dto;

public class UserResponse {

    private long id;
    private String cardNumber;
    private String name;
    private String surname;
    private String email;
    private int age;

    public long getId() {
        return id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }

    private UserResponse(long id, String cardNumber, String name, String surname, String email, int age) {
        this.id = id;
        this.cardNumber = cardNumber;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.age = age;
    }

    public static class Builder {
        private long id;
        private String cardNumber;
        private String name;
        private String surname;
        private String email;
        private int age;

        public UserResponse.Builder setId(long id) {
            this.id = id;
            return this;
        }
        public UserResponse.Builder setCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
            return this;
        }

        public UserResponse.Builder setName(String name) {
            this.name = name;
            return this;
        }

        public UserResponse.Builder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public UserResponse.Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public UserResponse.Builder setAge(int age) {
            this.age = age;
            return this;
        }


        public UserResponse build() {
            return new UserResponse(id,cardNumber,name,surname,email,age);
        }
    }
}
