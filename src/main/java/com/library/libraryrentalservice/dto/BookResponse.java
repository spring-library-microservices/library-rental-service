package com.library.libraryrentalservice.dto;

import com.library.libraryrentalservice.model.Author;
import com.library.libraryrentalservice.model.Category;

import java.util.Set;

public class BookResponse {

    private long id;
    private String title;
    private boolean isAvailable;
    private Set<Author> authors;
    private Set<Category> categories;
    private String currentBorrowerCardNumber;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public boolean getAvailable() {
        return isAvailable;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public String getCurrentBorrower() {
        return currentBorrowerCardNumber;
    }

    private BookResponse(long id, String title,
                         boolean isAvailable,
                         Set<Author> authors,
                         Set<Category> categories,
                         String currentBorrowerCardNumber) {
        this.id = id;
        this.title = title;
        this.isAvailable = isAvailable;
        this.authors = authors;
        this.categories = categories;
        this.currentBorrowerCardNumber = currentBorrowerCardNumber;
    }

    public static class Builder {
        private long id;
        private String title;
        private boolean isAvailable;
        private Set<Author> authors;
        private Set<Category> categories;
        private String currentBorrowerCardNumber;

        public Builder setId(long id) {
            this.id = id;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setIsAvailable(boolean isAvailable) {
            this.isAvailable = isAvailable;
            return this;
        }

        public Builder setAuthors(Set<Author> authors) {
            this.authors = authors;
            return this;
        }

        public Builder setCategories(Set<Category> categories) {
            this.categories = categories;
            return this;
        }

        public Builder setCurrentBorrower(String currentBorrowerCardNumber) {
            this.currentBorrowerCardNumber = currentBorrowerCardNumber;
            return this;
        }

        public BookResponse build() {
            return new BookResponse(id, title, isAvailable, authors, categories, currentBorrowerCardNumber);
        }
    }
}
