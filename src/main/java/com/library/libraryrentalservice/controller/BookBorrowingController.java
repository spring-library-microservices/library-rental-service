package com.library.libraryrentalservice.controller;

import com.library.libraryrentalservice.service.BookBorrowingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/books")
public class BookBorrowingController {

    private final BookBorrowingService bookBorrowingService;

    public BookBorrowingController(BookBorrowingService bookBorrowingService) {
        this.bookBorrowingService = bookBorrowingService;
    }

    @PostMapping(path = "/borrow/{bookId}")
    public ResponseEntity<String> borrowBook(@PathVariable("bookId") long bookId, HttpServletRequest httpServletRequest) {

        long userId = Long.parseLong(httpServletRequest.getHeader("userId"));

        bookBorrowingService.borrowBook(bookId, userId);

        return new ResponseEntity<>("Book borrowed successfully!", HttpStatus.OK);
    }

    @PostMapping(path = "/return/{bookId}")
    public ResponseEntity<String> returnBook(@PathVariable("bookId") long bookId, HttpServletRequest httpServletRequest) {

        long userId = Long.parseLong(httpServletRequest.getHeader("userId"));

        bookBorrowingService.returnBook(bookId, userId);

        return new ResponseEntity<>("Book returned successfully!", HttpStatus.OK);
    }
}
