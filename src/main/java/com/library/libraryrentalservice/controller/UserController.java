package com.library.libraryrentalservice.controller;

import com.library.libraryrentalservice.dto.UserResponse;
import com.library.libraryrentalservice.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(path = "/books/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/no-borrowing-record")
    public Set<UserResponse> getUsersWithNoBorrowingRecord() {
        return userService.getUsersWithNoBorrowingRecord();
    }
}
