package com.library.libraryrentalservice.controller;

import com.library.libraryrentalservice.dto.BookResponse;
import com.library.libraryrentalservice.model.AgeRange;
import com.library.libraryrentalservice.service.BookService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(path = "/books")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<BookResponse> getAllBooksInfo() {
        return bookService.getAllBooks();
    }

    @GetMapping(path = "/by-category", produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<BookResponse> getBooksByCategory(@RequestParam Set<String> booksCategories) {
        return bookService.getBooksByCategory(booksCategories);
    }

    @GetMapping(path = "/on-hands")
    public Set<BookResponse> getBooksWithOnHandStatus() {
        return bookService.getBooksWithOnHandsStatus();
    }

    @GetMapping(path = "/overdue")
    public Set<BookResponse> getBooksWithOverdueStatus() {
        return bookService.getBooksWithOverdueStatus();
    }

    @GetMapping(path = "/most-popular/year/top5")
    public Set<BookResponse> getTop5PopularBooksInYear() {
        return bookService.getTop5PopularBooksInYear();
    }

    @GetMapping(path = "/age-range")
    public Set<BookResponse> getBooksForAgeRange(@RequestParam AgeRange ageRange) {
        return bookService.getBooksForAgeRange(ageRange);
    }




}
