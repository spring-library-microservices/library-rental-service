package com.library.libraryrentalservice.controller;

import com.library.libraryrentalservice.dto.AuthorResponse;
import com.library.libraryrentalservice.model.AgeRange;
import com.library.libraryrentalservice.service.AuthorService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/books/author")
public class AuthorController {

    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping( path = "/most-popular", produces = MediaType.APPLICATION_JSON_VALUE)
    public AuthorResponse getMostPopularAuthorForAgeGroup(@RequestParam AgeRange ageRange) {
        return authorService.getMostPopularAuthorForAgeGroup(ageRange);
    }
}
