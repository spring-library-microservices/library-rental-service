package com.library.libraryrentalservice.repository;

import com.library.libraryrentalservice.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {


    @Query(value = "SELECT * FROM authors " +
            "LEFT JOIN books_authors ba on authors.id = ba.author_id " +
            "LEFT JOIN books b on b.id = ba.book_id " +
            "LEFT JOIN borrowing_registry rr on b.id = rr.book_id " +
            "LEFT JOIN users u on u.id = rr.users_id " +
            "WHERE u.age BETWEEN :ageRangeStart AND :ageRangeEnd " +
            "group by author_id ORDER BY count(author_id) DESC LIMIT 1"
            , nativeQuery = true)
    Author findMostPopularAuthorForAgeGroup(@Param("ageRangeStart") int rangeStart, @Param("ageRangeEnd") int rangeEnd);


}
