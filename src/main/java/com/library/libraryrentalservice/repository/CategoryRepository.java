package com.library.libraryrentalservice.repository;


import com.library.libraryrentalservice.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Long> {

    @Query(value = "select type from categories", nativeQuery = true)
    Set<String> findAllCategories();
}
