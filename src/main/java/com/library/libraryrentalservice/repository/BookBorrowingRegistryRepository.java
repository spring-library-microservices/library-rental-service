package com.library.libraryrentalservice.repository;


import com.library.libraryrentalservice.model.BookBorrowingRegistry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Repository
public interface BookBorrowingRegistryRepository extends JpaRepository<BookBorrowingRegistry,Long> {

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO borrowing_registry(borrowing_date, due_date,book_id, users_id)\n" +
            "VALUES (:borrowingDate,:dueDate,:bookId,:userId);",nativeQuery = true)
    void saveBorrowingRecord(@Param("borrowingDate")LocalDate borrowingDate,
                    @Param("dueDate")LocalDate dueDate,
                    @Param("bookId") long bookId,
                    @Param("userId") long userId);

}

