package com.library.libraryrentalservice.repository;

import com.library.libraryrentalservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Set;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM users u WHERE u.id = :id", nativeQuery = true)
    User findUserById(@Param("id") long id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE users u set  book_counter=:bookCount where u.id = :user",
            nativeQuery = true)
    void updateUserBookCounter(@Param("user") User user, @Param("bookCount") int userBookCount);


    @Query(value = "SELECT * FROM users LEFT JOIN borrowing_registry rr on users.id = rr.users_id where rr.users_id IS NULL ",
            nativeQuery = true)
    Set<User> usersWithNoBorrowingRecord();
}
