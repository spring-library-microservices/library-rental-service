package com.library.libraryrentalservice.repository;

import com.library.libraryrentalservice.model.Book;
import com.library.libraryrentalservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Set;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    @Query(value = "SELECT * FROM books u WHERE u.id = :id", nativeQuery = true)
    Book findBookById(@Param("id") long id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE books b set is_available =:isAvailable,current_borrower_id=:user,borrowing_date=:borrowing_date where b.id = :book",
            nativeQuery = true)
    void changeAvailabilityAndCurrentBorrower(@Param("isAvailable") boolean isAvailable,
                                              @Param("book") Book book,
                                              @Param("user") User user,
                                              @Param("borrowing_date") LocalDate borrowingDate);

    @Query(value = "select * from books", nativeQuery = true)
    Set<Book> findAllBooks();


    @Query(value = "SELECT * FROM books LEFT JOIN books_categories bc on books.id = bc.book_id " +
            "LEFT JOIN categories c on c.id = bc.category_id" +
            " where c.type in :cat", nativeQuery = true)
    Set<Book> findAllBooksByCategory(@Param("cat") Set<String> booksCategories);


    @Query(value = "SELECT * FROM books u WHERE is_available is false", nativeQuery = true)
    Set<Book> findAllOnHandsBooks();


    @Query(value = "SELECT * FROM books b WHERE DATE_ADD(borrowing_date,INTERVAL 30 day)<current_date", nativeQuery = true)
    Set<Book> findAllOverdueBooks();


    @Query(value = "SELECT * fROM books LEFT JOIN borrowing_registry rr on books.id = rr.book_id " +
            "WHERE rr.borrowing_date>=DATE_SUB(CURRENT_DATE,interval 1 year) " +
            "GROUP BY book_id ORDER BY count(book_id) desc LIMIT 5"
            , nativeQuery = true)
    Set<Book> findTop5PopularBooks();


    @Query(value = "SELECT * FROM books " +
            "LEFT OUTER JOIN users u on u.id = books.current_borrower_id " +
            "WHERE u.age BETWEEN :ageRangeStart AND :ageRangeEnd", nativeQuery = true)
    Set<Book> findBookByAgeRange(@Param("ageRangeStart") int rangeStart, @Param("ageRangeEnd") int rangeEnd);

}
