package com.library.libraryrentalservice.mapper;

import com.library.libraryrentalservice.dto.BookResponse;
import com.library.libraryrentalservice.model.Book;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class BookMapper {

    public BookResponse toDto(Book book) {

        return new BookResponse.Builder()
                .setId(book.getId())
                .setTitle(book.getTitle())
                .setCategories(book.getCategories())
                .setAuthors(book.getAuthors())
                .setCurrentBorrower(Objects
                        .nonNull(book.getCurrentBorrower())
                        ? book.getCurrentBorrower().getCardNumber() : null)
                .setIsAvailable(book.isAvailable())
                .build();
    }

}

