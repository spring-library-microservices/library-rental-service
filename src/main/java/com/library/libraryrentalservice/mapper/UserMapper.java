package com.library.libraryrentalservice.mapper;

import com.library.libraryrentalservice.dto.UserResponse;
import com.library.libraryrentalservice.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public UserResponse toDto(User user){

        return new UserResponse.Builder()
                .setId(user.getId())
                .setCardNumber(user.getCardNumber())
                .setName(user.getName())
                .setSurname(user.getSurname())
                .setEmail(user.getEmail())
                .setAge(user.getAge())
                .build();
    }
}
