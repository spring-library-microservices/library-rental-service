package com.library.libraryrentalservice.mapper;

import com.library.libraryrentalservice.dto.AuthorResponse;
import com.library.libraryrentalservice.model.Author;
import org.springframework.stereotype.Component;

@Component
public class AuthorMapper {

    public AuthorResponse toDto(Author author){

        return new AuthorResponse.Builder()
                .setId(author.getId())
                .setName(author.getName())
                .setSurname(author.getSurname())
                .build();
    }
}
